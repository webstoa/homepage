import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { HomepageViewDataService } from '../../services/homepage-view-data.service';


@Component({
  selector: 'ws-homepage-bio',
  templateUrl: './homepage-bio.component.html',
  styleUrls: ['./homepage-bio.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomepageBioComponent implements OnInit {

  constructor(private viewDataService: HomepageViewDataService) {
  }

  ngOnInit() {
  }

}
