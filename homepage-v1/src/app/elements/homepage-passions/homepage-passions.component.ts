import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { HomepageViewDataService } from '../../services/homepage-view-data.service';


@Component({
  selector: 'ws-homepage-passions',
  templateUrl: './homepage-passions.component.html',
  styleUrls: ['./homepage-passions.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomepagePassionsComponent implements OnInit {

  constructor(private viewDataService: HomepageViewDataService) {
  }

  ngOnInit() {
  }

}
