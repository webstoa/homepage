import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepagePassionsComponent } from './homepage-passions.component';

describe('HomepagePassionsComponent', () => {
  let component: HomepagePassionsComponent;
  let fixture: ComponentFixture<HomepagePassionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepagePassionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepagePassionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
