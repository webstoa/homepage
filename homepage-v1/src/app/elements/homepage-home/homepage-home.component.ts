import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { HomepageViewDataService } from '../../services/homepage-view-data.service';


@Component({
  selector: 'ws-homepage-home',
  templateUrl: './homepage-home.component.html',
  styleUrls: ['./homepage-home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomepageHomeComponent implements OnInit {

  constructor(private viewDataService: HomepageViewDataService) {
  }

  ngOnInit() {
  }

}
