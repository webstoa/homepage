import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageHobbiesComponent } from './homepage-hobbies.component';

describe('HomepageHobbiesComponent', () => {
  let component: HomepageHobbiesComponent;
  let fixture: ComponentFixture<HomepageHobbiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageHobbiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageHobbiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
