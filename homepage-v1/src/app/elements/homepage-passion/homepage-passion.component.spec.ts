import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepagePassionComponent } from './homepage-passion.component';

describe('HomepagePassionComponent', () => {
  let component: HomepagePassionComponent;
  let fixture: ComponentFixture<HomepagePassionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepagePassionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepagePassionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
