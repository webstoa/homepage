import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { HomepageViewDataService } from '../../services/homepage-view-data.service';


@Component({
  selector: 'ws-homepage-projects',
  templateUrl: './homepage-projects.component.html',
  styleUrls: ['./homepage-projects.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomepageProjectsComponent implements OnInit {

  constructor(private viewDataService: HomepageViewDataService) {
  }

  ngOnInit() {
  }

}
