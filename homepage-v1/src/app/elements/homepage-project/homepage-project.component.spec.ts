import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomepageProjectComponent } from './homepage-project.component';

describe('HomepageProjectComponent', () => {
  let component: HomepageProjectComponent;
  let fixture: ComponentFixture<HomepageProjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomepageProjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomepageProjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
