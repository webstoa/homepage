import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';

import { MaterialComponentsModule } from './material-components.module';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { HomepagePageComponent } from './pages/homepage-page/homepage-page.component';
import { ProjectsPageComponent } from './pages/projects-page/projects-page.component';
import { BiographyPageComponent } from './pages/biography-page/biography-page.component';
import { StoryPageComponent } from './pages/story-page/story-page.component';
import { HomepageHomeComponent } from './elements/homepage-home/homepage-home.component';
import { HomepageProjectsComponent } from './elements/homepage-projects/homepage-projects.component';
import { HomepageProjectComponent } from './elements/homepage-project/homepage-project.component';
import { HomepageBioComponent } from './elements/homepage-bio/homepage-bio.component';
import { HomepagePassionsComponent } from './elements/homepage-passions/homepage-passions.component';
import { HomepagePassionComponent } from './elements/homepage-passion/homepage-passion.component';
import { HomepageHobbiesComponent } from './elements/homepage-hobbies/homepage-hobbies.component';
import { HomepageHobbyComponent } from './elements/homepage-hobby/homepage-hobby.component';


@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    HomepagePageComponent,
    ProjectsPageComponent,
    BiographyPageComponent,
    StoryPageComponent,
    HomepageHomeComponent,
    HomepageProjectsComponent,
    HomepageProjectComponent,
    HomepageBioComponent,
    HomepagePassionsComponent,
    HomepagePassionComponent,
    HomepageHobbiesComponent,
    HomepageHobbyComponent
  ],
  imports: [
    BrowserModule,
    FormsModule, ReactiveFormsModule,
    FlexLayoutModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    ),
    MaterialComponentsModule,
    AppRoutingModule
  ],
  providers: [
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
