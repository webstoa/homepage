import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ws-biography-page',
  templateUrl: './biography-page.component.html',
  styleUrls: ['./biography-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BiographyPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
