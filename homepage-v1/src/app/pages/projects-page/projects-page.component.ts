import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ws-projects-page',
  templateUrl: './projects-page.component.html',
  styleUrls: ['./projects-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProjectsPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
