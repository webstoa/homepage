import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'ws-story-page',
  templateUrl: './story-page.component.html',
  styleUrls: ['./story-page.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class StoryPageComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
